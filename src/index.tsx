import * as React from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./app";
const APP = (
    <BrowserRouter>
        <App />
    </BrowserRouter>
);
render(
    APP,
    document.getElementById("root"),
);
