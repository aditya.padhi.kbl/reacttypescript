import * as React from "react";
import { Link, Route } from "react-router-dom";
import asyncComponent from "./hoc/asyncComponent";
import { ICountry } from "./interface";
const ASYNCPLACE = asyncComponent( () => import("./place"));

const ASYNCCOUNTRY: React.SFC<ICountry> = asyncComponent( () => import("./country"));

class App extends React.Component {
    public render() {
        return (
            <React.Fragment>
                <Link to="/country">Country</Link>|<Link to="/place">Place</Link>|
                <Route path="/place" exact render = { (props) => <ASYNCPLACE {...props} ></ASYNCPLACE>} />
                <Route path="/country" exact render = {
                    (props) => <ASYNCCOUNTRY countryName="India" {...props}></ASYNCCOUNTRY>} />
                <Route path="/" exact component={ASYNCPLACE} />
            </React.Fragment>
        );
    }
}

export default App;
