import * as React from "react";

interface IAsyncComponentState {
    Component: any;
}

export default function asyncComponent(component: any): any {
    return class extends React.Component<{}, IAsyncComponentState> {
        constructor(props: any) {
            super(props);
            this.state = {
                Component: null,
            };
        }

        public async componentDidMount() {
            const { default: Component} = await component();
            this.setState({Component});
        }

        public render() {
            const C = this.state.Component;
            return C ? <C {...this.props}/> : <div>... Loading</div>;
        }
    };
}
