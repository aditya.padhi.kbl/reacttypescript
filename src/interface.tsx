import { RouteComponentProps } from "react-router";

export interface ICountry extends RouteComponentProps {
    countryName: string;
}
