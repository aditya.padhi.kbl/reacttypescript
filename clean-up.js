(function ( ) {
    const del  = require("del");
    del(['src',
        '.babelrc',
        '.gitignore',
        '*.yml',
        'bitbucket*',
        'empty-module.js',
        'jest.config.js',
        'tsconfig.json',
        'tslint.json',
        'webpack.*'])
})();