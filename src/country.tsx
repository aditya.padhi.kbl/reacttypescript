import Button from "@material-ui/core/Button";
import * as React from "react";
import { ICountry } from "./interface";
const Country: React.StatelessComponent<ICountry> = (props) => {
    return (
        <React.Fragment>
            <h2>India is great!!!!! {props.countryName} Really!!!!</h2>
            <Button variant="contained" color="primary">Click Here</Button>
        </React.Fragment>
    );
};

export default Country;
